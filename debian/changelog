libmath-vec-perl (1.01-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libmath-vec-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 01:46:42 +0100

libmath-vec-perl (1.01-4) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Wrap long lines in changelog entries: 1.01-1, 0.04-2.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 19:13:05 +0100

libmath-vec-perl (1.01-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sun, 03 Jan 2021 14:44:45 +0100

libmath-vec-perl (1.01-3) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Axel Beckert ]
  * debian/copyright: Use David Moreno Garza's @debian.org address

  [ Niko Tyni ]
  * Declare the package autopkgtestable
  * Update to Standards-Version 3.9.6
  * Add explicit build dependency on libmodule-build-perl

 -- Niko Tyni <ntyni@debian.org>  Mon, 08 Jun 2015 21:03:07 +0300

libmath-vec-perl (1.01-2) unstable; urgency=low

  * Team upload

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * Refresh debian/rules, no functional changes; except: don't create
    .packlist file any more.
  * debian/watch: use dist-based URL.
  * debian/copyright: wrap a long line.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * Change my email address.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * Fix dpkg-source error "duplicate field Standards-Version found"
  * Bump debhelper compatibility to 9
    + Update versioned debhelper build-dependency accordingly
    + Fixes package-uses-deprecated-debhelper-compat-version warning
  * Fix the following lintian warnings:
    + package-has-a-duplicate-build-relation
    + using-first-person-in-description
  * Switch to source format "3.0 (quilt)"
  * Drop build-dependencies obsoleted by 1.01 upstream release
    + Remove POD-tests related environment variables from debian/rules
  * Revamp debian/rules:
    + Fix lintian warning debian-rules-missing-recommended-target
    + Replace "dh_clean -k" with "dh_prep"
    + Use dh_auto_{configure,build,test,install,clean}
    + Drop obsolete parameters from dh_{clean,installchangelogs}
    + Remove obsolete variable usage
    + Finally switch to a minimal dh-style debian/rules file
  * No more install README (contains installation instructions)
  * Bump Standards-Version to 3.9.5 (no further changes)

 -- Axel Beckert <abe@debian.org>  Mon, 23 Dec 2013 02:31:17 +0100

libmath-vec-perl (1.01-1) unstable; urgency=low

  * New upstream release
  * Moving libmodule-build-perl from build-depends-indep to build-depends
    (Policy 7.6)

 -- Gunnar Wolf <gwolf@debian.org>  Fri, 15 Jun 2007 13:50:33 -0500

libmath-vec-perl (0.04-2) unstable; urgency=low

  * New Maintainer: Debian Perl Group.
  * Add libtest-pod-perl and libtest-pod-coverage-perl to build dependencies.
  * Add environment variables to debian/rules to active pod and pod-coverage
    tests.
  * Add watch file.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri, 15 Sep 2006 00:32:19 +0200

libmath-vec-perl (0.04-1) unstable; urgency=low

  * Initial Release.

 -- David Moreno Garza <damog@debian.org>  Mon,  5 Jun 2006 21:48:38 -0500
